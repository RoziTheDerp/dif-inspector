package com.matt.difinspector.map;

import java.util.ArrayList;
import java.util.List;

import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.ConvexHull;

public class Brush
{
	protected List<BrushPlane> planes;
	
	protected List<Point3F> points;
	
	protected ConvexHull hull;
	
	public Brush(ConvexHull hull)
	{
		this.hull = hull;
		this.planes = new ArrayList<BrushPlane>();
		this.points = new ArrayList<Point3F>();
	}
	
	public void addPlane(BrushPlane plane)
	{
		this.planes.add(plane);
	}
	
	public void addPoint(Point3F point)
	{
		// well you got a point...
		this.points.add(point);
	}
	
	public List<BrushPlane> getPlanes()
	{
		return this.planes;
	}
	
	public List<Point3F> getPoints()
	{
		return this.points;
	}
	
	public ConvexHull getHull() {
		return this.hull;
	}
}
