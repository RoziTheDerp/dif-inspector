package com.matt.difinspector.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;

public class Settings
{
	private static String lastOpenDir = "";
	private static String lastSaveDir = "";
	private static final int VERSION = 0;
	
	public static void loadConfig()
	{
		File configFile = new File(Util.getAppDir(), "config.cfg");
		if (!configFile.exists())
		{
			Settings.saveConfig();
			return;
		}
		
		try
		{
			ReverseDataInputStream dis = new ReverseDataInputStream(new FileInputStream(configFile));
			
			int magic1 = dis.readUnsignedByte();
			int magic2 = dis.readUnsignedByte();
			int magic3 = dis.readUnsignedByte();
			int magic4 = dis.readUnsignedByte();
			
			if (magic1 != 'D' || magic2 != 'I' || magic3 != 'F' || magic4 != 'C')
			{
				dis.close();
				return;
			}
			
			int readVersion = dis.readInt();
			if (readVersion != Settings.VERSION)
			{
				dis.close();
				return;
			}
			
			Settings.lastOpenDir = dis.readString();
			Settings.lastSaveDir = dis.readString();
			
			dis.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static  void saveConfig()
	{
		File configFile = new File(Util.getAppDir(), "config.cfg");
		try
		{
			ReverseDataOutputStream dos = new ReverseDataOutputStream(new FileOutputStream(configFile));
			
			dos.writeByte('D');
			dos.writeByte('I');
			dos.writeByte('F');
			dos.writeByte('C');
			
			dos.writeInt(Settings.VERSION);
			
			dos.writeString(Settings.lastOpenDir);
			dos.writeString(Settings.lastSaveDir);
			
			dos.flush();
			dos.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void setLastOpenDir(String lastOpenDir)
	{
		Settings.lastOpenDir = lastOpenDir;
	}
	
	public static String getLastOpenDir()
	{
		return Settings.lastOpenDir;
	}
	
	public static void setLastSaveDir(String lastSaveDir)
	{
		Settings.lastSaveDir = lastSaveDir;
	}
	
	public static String getLastSaveDir()
	{
		return Settings.lastSaveDir;
	}
}
