package com.matt.difinspector.mesh;

import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.util.Util;
import com.matt.jmatt.lists.OptimizedList;

public class MeshBuilder
{
	protected OptimizedList<Point3F> positions;
	protected OptimizedList<Point2F> textureCoordinates;
	protected OptimizedList<String> textures;
	protected String defaultTexture;
	
	public MeshBuilder(String defaultTexture)
	{
		this.positions = new OptimizedList<Point3F>();
		this.textureCoordinates = new OptimizedList<Point2F>();
		this.textures = new OptimizedList<String>();
		
		this.defaultTexture = defaultTexture;
	}
	
	public void addVertex(Point3F vertex, Point2F uv, String texture)
	{
		if (texture.contains("."))
			texture = texture.substring(0, texture.lastIndexOf("."));
		
		if (texture.equalsIgnoreCase("material"))
		{
			this.addVertex(vertex, uv);
			return;
		}
		this.positions.add(vertex);
		this.textureCoordinates.add(uv);
		this.textures.add(texture);
	}
	
	public void addVertex(Point3F vertex, Point2F uv)
	{
		String lastTexture = textures.lastObject();
		if (lastTexture == null)
			lastTexture = this.defaultTexture;
		this.addVertex(vertex, uv, lastTexture);
	}
	
	public void addVertex(Point3F vertex)
	{
		Point2F lastUV = this.textureCoordinates.lastObject();
		if (lastUV == null)
			lastUV = new Point2F();
		this.addVertex(vertex, lastUV);
	}
	
	public Mesh build()
	{
		Point3F[] vertices = new Point3F[this.positions.sizeObjects()];
		Integer[] indices = new Integer[this.positions.sizeIndices()];
		this.positions.getObjects().toArray(vertices);
		this.positions.getIndices().toArray(indices);
		
		Point2F[] textureCoords = new Point2F[this.textureCoordinates.sizeObjects()];
		Integer[] textureCoordIndices = new Integer[this.textureCoordinates.sizeIndices()];
		this.textureCoordinates.getObjects().toArray(textureCoords);
		this.textureCoordinates.getIndices().toArray(textureCoordIndices);
		
		String[] textures = new String[this.textures.sizeObjects()];
		Integer[] textureIndices = new Integer[this.textures.sizeIndices()];
		this.textures.getObjects().toArray(textures);
		this.textures.getIndices().toArray(textureIndices);
		
		Mesh mesh = new Mesh();
		
		mesh.setVertices(vertices, Util.getIntArray(indices));
		mesh.setTextureCoords(textureCoords, Util.getIntArray(textureCoordIndices));
		mesh.setTextures(textures, Util.getIntArray(textureIndices));
		
		return mesh;
	}
	
}
