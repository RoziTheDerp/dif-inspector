package com.matt.difinspector.interior;

import com.matt.difinspector.util.Util;

public enum EnumLightFlags
{
	AnimationAmbient(Util.BIT(0)),
	AnimationLoop(Util.BIT(1)),
	AnimationFlicker(Util.BIT(2)),
	AnimationTypeMask(Util.BIT(3) - 1),
	AlarmLight(Util.BIT(3));
	
	protected int value;
	
	private EnumLightFlags(int value)
	{
		this.value = value;
	}
	
	public int getValue()
	{
		return this.value;
	}
}
