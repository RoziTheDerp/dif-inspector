package com.matt.difinspector.structures;

import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.util.Util;

public class ItrPaddedPoint
{
	private Point3F point;
	private float fogCoord;
	private byte[] fogColor;
	
	public ItrPaddedPoint(Point3F point, float fogCoord, byte[] fogColor)
	{
		this.point = point;
		this.fogCoord = fogCoord;
		this.fogColor = fogColor;
	}
	
	public ItrPaddedPoint(Point3F point)
	{
		this.point = point;
		this.fogColor = new byte[4];
	}
	
	public void setPoint(Point3F point)
	{
		this.point = point;
	}
	
	public Point3F getPoint()
	{
		return this.point;
	}
	
	public void setFogCoord(float fogCoord)
	{
		this.fogCoord = fogCoord;
	}
	
	public float getFogCoord()
	{
		return this.fogCoord;
	}
	
	public void setFogColor(byte[] fogColor)
	{
		this.fogColor = fogColor;
	}
	
	public byte[] getFogColor()
	{
		return this.fogColor;
	}
	
	@Override
	public String toString()
	{
		return "(ItrPaddedPoint){point: " + this.point + ", fogCoord: " + this.fogCoord + ", fogColor: " + Util.getArrayString(this.fogColor) + "}";
	}
}
