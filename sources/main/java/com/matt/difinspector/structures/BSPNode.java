package com.matt.difinspector.structures;

public class BSPNode
{
	private short planeIndex;
	private int frontIndex;
	private int backIndex;
	private short terminalZone;
	
	public BSPNode(short planeIndex, int frontIndex, int backIndex, short terminalZone)
	{
		this.planeIndex = planeIndex;
		this.frontIndex = frontIndex;
		this.backIndex = backIndex;
		this.terminalZone = terminalZone;
	}
	
	public BSPNode(short planeIndex, int frontIndex, int backIndex)
	{
		this(planeIndex, frontIndex, backIndex, (short)0);
	}
	
	public short getPlaneIndex()
	{
		return this.planeIndex;
	}
	
	public int getFrontIndex()
	{
		return this.frontIndex;
	}
	
	public int getBackIndex()
	{
		return this.backIndex;
	}
	
	public short getTerminalZone()
	{
		return this.terminalZone;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof BSPNode))
			return false;
		
		BSPNode bspNode = (BSPNode)o;
		
		return this.planeIndex == bspNode.planeIndex && this.frontIndex == bspNode.frontIndex && this.backIndex == bspNode.backIndex && this.terminalZone == bspNode.terminalZone;
	}
	
	@Override
	public String toString()
	{
		return "(BSPNode){PlaneIndex: " + this.planeIndex + ", FrontIndex: " + this.frontIndex + ", BackIndex: " + this.backIndex + ", TerminalZone: " + this.terminalZone + "}";
	}
}
