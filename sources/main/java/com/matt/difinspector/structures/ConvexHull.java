package com.matt.difinspector.structures;

public class ConvexHull
{
	private float minX, maxX, minY, maxY, minZ, maxZ;
	private int hullStart, surfaceStart, planeStart;
	private short hullCount, surfaceCount;
	private int polyListPlaneStart;
	private int polyListPointStart;
	private int polyListStringStart;
	private short searchTag;
	private boolean staticMesh;
	
	public ConvexHull(float minX, float maxX, float minY, float maxY, float minZ, float maxZ, int hullStart, int surfaceStart, int planeStart, short hullCount, short surfaceCount, int polyListPlaneStart, int polyListPointStart, int polyListStringStart, short searchTag, boolean staticMesh)
	{
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		this.minZ = minZ;
		this.maxZ = maxZ;
		this.hullStart = hullStart;
		this.surfaceStart = surfaceStart;
		this.planeStart = planeStart;
		this.hullCount = hullCount;
		this.surfaceCount = surfaceCount;
		this.polyListPlaneStart = polyListPlaneStart;
		this.polyListPointStart = polyListPointStart;
		this.polyListStringStart = polyListStringStart;
		this.searchTag = searchTag;
		this.staticMesh = staticMesh;
	}
	
	public float getMinX()
	{
		return this.minX;
	}
	
	public void setMaxX(float maxX)
	{
		this.maxX = maxX;
	}
	
	public void setMinX(float minX)
	{
		this.minX = minX;
	}

	public void setMinY(float minY)
	{
		this.minY = minY;
	}

	public void setMaxY(float maxY)
	{
		this.maxY = maxY;
	}

	public void setMinZ(float minZ)
	{
		this.minZ = minZ;
	}

	public void setMaxZ(float maxZ)
	{
		this.maxZ = maxZ;
	}

	public float getMaxX()
	{
		return this.maxX;
	}
	
	public float getMinY()
	{
		return this.minY;
	}
	
	public float getMaxY()
	{
		return this.maxY;
	}
	
	public float getMinZ()
	{
		return this.minZ;
	}
	
	public float getMaxZ()
	{
		return this.maxZ;
	}
	
	public int getHullStart()
	{
		return this.hullStart;
	}
	
	public int getSurfaceStart()
	{
		return this.surfaceStart;
	}
	
	public int getPlaneStart()
	{
		return this.planeStart;
	}
	
	public short getHullCount()
	{
		return this.hullCount;
	}
	
	public short getSurfaceCount()
	{
		return this.surfaceCount;
	}
	
	public int getPolyListPlaneStart()
	{
		return this.polyListPlaneStart;
	}
	
	public int getPolyListPointStart()
	{
		return this.polyListPointStart;
	}
	
	public int getPolyListStringStart()
	{
		return this.polyListStringStart;
	}
	
	public void setPolyListStringStart(int polyListStringStart)
	{
		this.polyListStringStart = polyListStringStart;
	}
	
	public short getSearchTag()
	{
		return this.searchTag;
	}
	
	public boolean getStaticMesh()
	{
		return this.staticMesh;
	}
	
	@Override
	public String toString()
	{
		return "(ConvexHull){minX: " + this.minX + ", maxX: " + this.maxX + ", minY: " + this.minY + ", maxY: " + this.maxY + ", minZ: " + this.minZ + ", maxZ: " + this.maxZ + ", hullStart: " + this.hullStart + ", surfaceStart: " + this.surfaceStart + ", planeStart: " + this.planeStart + ", hullCount: " + this.hullCount + ", surfaceCount: " + this.surfaceCount + ", polyListPlaneStart: " + this.polyListPlaneStart + ", polyListPointStart: " + this.polyListPointStart + ", polyListStringStart: " + this.polyListStringStart + ", searchTag: " + this.searchTag + ", staticMesh: " + this.staticMesh + "}";
	}
}
