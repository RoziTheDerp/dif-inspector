package com.matt.difinspector.structures;

public class BSPSolidLeaf
{
	private int surfaceIndex;
	private short surfaceCount;
	
	public BSPSolidLeaf(int surfaceIndex, short surfaceCount)
	{
		this.surfaceIndex = surfaceIndex;
		this.surfaceCount = surfaceCount;
	}
	
	public int getSurfaceIndex()
	{
		return this.surfaceIndex;
	}
	
	public short getSurfaceCount()
	{
		return this.surfaceCount;
	}
	
	@Override
	public String toString()
	{
		return "(BSPSolidLeaf){surfaceIndex: " + this.surfaceIndex + ", surfaceCount: " + this.surfaceCount + "}";
	}
}
