package com.matt.difinspector.structures;

public class TexMatrix
{
	private int T, N, B;
	
	public TexMatrix()
	{
		this.T = -1;
		this.N = -1;
		this.B = -1;
	}
	
	public TexMatrix(int T, int N, int B)
	{
		this.T = T;
		this.N = N;
		this.B = B;
	}
	
	public int getT()
	{
		return this.T;
	}
	
	public int getN()
	{
		return this.N;
	}
	
	public int getB()
	{
		return this.B;
	}
	
	@Override
	public String toString()
	{
		return "(TexMatrix){T: " + this.T + ", N: " + this.N + ", B: " + this.B + "}";
	}
}
