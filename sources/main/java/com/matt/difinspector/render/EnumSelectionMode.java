package com.matt.difinspector.render;

public enum EnumSelectionMode
{
	SURFACES,
	POINTS,
	HULLS,
	PLANES,
	NONE;
}
