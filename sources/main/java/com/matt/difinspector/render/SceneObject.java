package com.matt.difinspector.render;

import com.matt.difinspector.math.Transform;

public abstract class SceneObject
{
	protected Transform transform;

	public SceneObject()
	{
		this.transform = new Transform();
	}
	
	public abstract void update();
	public abstract void render();
	
	public Transform getTransform()
	{
		return transform;
	}

	public void setTransform(Transform transform)
	{
		this.transform = transform;
	}
}
