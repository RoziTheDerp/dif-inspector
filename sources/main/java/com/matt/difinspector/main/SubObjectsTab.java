package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.util.Settings;

public class SubObjectsTab extends JPanel
{
	private static final long serialVersionUID = -5578301654094737597L;
	
	private Map<String, SubPanel> panels;
	
	private JComboBox<String> levels;
	private JComboBox<String> menu;
	private JButton export;
	private SubPanel displayedPanel;
	
	public SubObjectsTab()
	{
		this.panels = new HashMap<String, SubPanel>();
		this.addPanels();
		
		this.setLayout(new BorderLayout());
		
		this.levels = new JComboBox<String>();
		
		this.levels.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					SubObjectsTab.this.setDisplay(menu.getSelectedItem().toString());
				}
			}
		});
		
		this.menu = new JComboBox<String>();
		for (String s : this.panels.keySet())
		{
			this.menu.addItem(s);
		}
		
		this.menu.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e)
			{
				if (e.getStateChange() == ItemEvent.SELECTED)
				{
					SubObjectsTab.this.setDisplay(e.getItem().toString());
				}
			}
		});
		
		this.export = new JButton("Export");
		this.export.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				SubObjectsTab.this.doExport();
			}
		});
		
		JPanel menus = new JPanel();
		
		menus.add(this.levels);
		menus.add(this.menu);
		menus.add(this.export);
		
		this.add(menus, BorderLayout.NORTH);
		
		this.update();
	}
	
	public void addPanels()
	{
		this.panels.put("Materials", new MaterialsPanel());
		this.panels.put("Edges", new EdgesPanel());
		this.panels.put("Surfaces", new SurfacesPanel());
		this.panels.put("Points", new PointsPanel());
		this.panels.put("General", new GeneralPanel());
		this.panels.put("Planes", new PlanesPanel());
	}
	
	public void setDisplay(String option)
	{
		if (this.displayedPanel != null)
		{
			this.remove(this.displayedPanel);
		}
		
		this.displayedPanel = this.panels.get(option);
		
		if (this.displayedPanel != null)
		{
			this.add(this.displayedPanel);
			if (DifInspector.getInstance() != null && DifInspector.getInstance().getInterior() != null)
				this.displayedPanel.update(DifInspector.getInstance().getInterior().getSubObjects().get(this.levels.getSelectedIndex()));
			this.displayedPanel.revalidate();
		} else {
			System.err.println("Could not find panel: " + option);
		}
	}
	
	public void update()
	{
		this.levels.removeAllItems();
		if (DifInspector.getInstance() != null && DifInspector.getInstance().getInterior() != null)
		{
			InteriorResource interior = DifInspector.getInstance().getInterior();
			for (int i = 0; i < interior.getSubObjects().size(); i++)
			{
				this.levels.addItem("Sub Object: " + i);
			}
		}
		
		if (this.levels.getItemCount() > 0)
			this.levels.setSelectedIndex(0);
		this.menu.setSelectedIndex(0);
		this.setDisplay(this.menu.getSelectedItem().toString());
	}
	
	public void doExport()
	{
		int objectIndex = this.levels.getSelectedIndex();
		Interior subObject = DifInspector.getInstance().getInterior().getSubObjects().get(objectIndex);
		
		InteriorResource res = new InteriorResource();
		res.getDetailLevels().add(subObject);
		
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Interior v" + 0 + " Files (.dif)", "dif"));
		
		if (!Settings.getLastSaveDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastSaveDir()));
		int option = chooser.showSaveDialog(this);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null)
		{
			if (res.write(file, 0))
			{
				JOptionPane.showMessageDialog(this, "Successfully Exported!");
			} else
			{
				JOptionPane.showMessageDialog(this, "Export Failed:\n" + res.getErrors().toString());
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastSaveDir(dir);
		Settings.saveConfig();
	}
}
