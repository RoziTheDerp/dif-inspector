package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.structures.Edge;
import com.matt.difinspector.util.Util;

public class EdgesPanel extends SubPanel
{
	private static final long serialVersionUID = -6923922097457175257L;
	
	private DefaultTableModel model;
	private JTable table;
	
	public EdgesPanel()
	{
		this.setLayout(new BorderLayout());

		this.model = new DefaultTableModel();
		
		
		this.table = new JTable(model);
		this.table.setFillsViewportHeight(true);
		
		JScrollPane scroll = new JScrollPane(table);
		scroll.setColumnHeader(null);
		this.add(scroll, BorderLayout.CENTER);
	}
	
	public void update(Interior interior)
	{
		Edge[] edges = interior.getEdges();
		
		Vector<String> tableHeader = new Vector<String>();
		tableHeader.addElement("Index");
		tableHeader.addElement("Vertex 1");
		tableHeader.addElement("Vertex 2");
		tableHeader.addElement("Face 1");
		tableHeader.addElement("Face 2");
		
		List<List<String>> data = new ArrayList<List<String>>();
		
		if (edges != null)
		{
			for (Edge edge : edges)
			{
				List<String> l = new ArrayList<String>();
				l.add("" + edge.getVertex1());
				l.add("" + edge.getVertex2());
				l.add("" + edge.getFace1());
				l.add("" + edge.getFace2());
				data.add(l);
			}
		}
		
		model.setDataVector(Util.stringListToStringVector(data), tableHeader);
	}
}
