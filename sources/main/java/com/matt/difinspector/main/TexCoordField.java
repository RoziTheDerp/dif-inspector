package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JComponent;

import com.matt.difinspector.math.Point2F;

public class TexCoordField extends JComponent
{
	private static final long serialVersionUID = 2237022076014617699L;
	
	private Point2Field offsetField;
	private Point2Field scaleField;
	private FloatField rotField;
	
	public TexCoordField(Point2F offset, Point2F scale, float rotation)
	{
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(175, 100));
		
		this.offsetField = new Point2Field("Offset: ", offset);
		this.scaleField = new Point2Field("Scale: ", scale);
		this.rotField = new FloatField(rotation);
		this.rotField.setMin(-7200);
		this.rotField.setMax(7200);
		
		this.add(this.offsetField);
		this.add(this.scaleField);
		this.add(this.rotField);
	}
	
	public TexCoordField()
	{
		this(new Point2F(), new Point2F(), 0);
	}
	
	public void setOffset(Point2F offset)
	{
		this.offsetField.setPoint(offset);
	}
	
	public Point2F getOffset()
	{
		return this.offsetField.getPoint();
	}
	
	public void setScale(Point2F scale)
	{
		this.scaleField.setPoint(scale);
	}
	
	public Point2F getScale()
	{
		return this.scaleField.getPoint();
	}
	
	public void setRotation(float rotation)
	{
		this.rotField.setValue(rotation);
	}
	
	public float getRotation()
	{
		return this.rotField.getValue();
	}
	
}
