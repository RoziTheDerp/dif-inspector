package com.matt.difinspector.main;

import java.util.EventListener;

public interface ValueChangedListener extends EventListener
{
	public void valueChanged(ValueChangedEvent event);
}
