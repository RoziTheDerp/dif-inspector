package com.matt.difinspector.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.matt.difinspector.math.Point2I;

public class Point2IField extends JComponent
{
	private static final long serialVersionUID = 4432082004476176516L;
	
	private JLabel label;
	private JTextField xField;
	private JTextField yField;
	
	private class PointActionListener implements KeyListener
	{
		@Override
		public void keyTyped(KeyEvent e)
		{
			ActionEvent event = new ActionEvent(Point2IField.this, ActionEvent.ACTION_PERFORMED, "Changed");
			//PointField.this.dispatchEvent();
			ActionListener[] listeners = Point2IField.this.listenerList.getListeners(ActionListener.class);
			for (int i = 0; i < listeners.length; i++)
			{
				listeners[i].actionPerformed(event);
			}
		}

		@Override
		public void keyPressed(KeyEvent e){}

		@Override
		public void keyReleased(KeyEvent e){}
	}
	
	public Point2IField(String label, Point2I point)
	{
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(175, 25));
		this.label = new JLabel(label);
		this.xField = new JTextField("" + point.getX(), 3);
		this.xField.addKeyListener(new PointActionListener());
		this.yField = new JTextField("" + point.getY(), 3);
		this.yField.addKeyListener(new PointActionListener());
		
		this.add(this.label);
		this.add(this.xField);
		this.add(this.yField);
	}
	
	public Point2IField(Point2I point)
	{
		this("", point);
	}
	
	public Point2IField(String label)
	{
		this(label, new Point2I());
	}
	
	public Point2IField()
	{
		this("");
	}
	
	public void setPoint(Point2I point)
	{
		this.xField.setText("" + point.getX());
		this.yField.setText("" + point.getY());
	}
	
	public Point2I getPoint()
	{
		int x;
		int y;
		
		try {
			x = Integer.parseInt(this.xField.getText());
		} catch (NumberFormatException e)
		{
			x = 0;
		}
		
		try {
			y = Integer.parseInt(this.yField.getText());
		} catch (NumberFormatException e)
		{
			y = 0;
		}
		
		return new Point2I(x, y);
	}
	
	public void addActionListener(ActionListener l) {
        this.listenerList.add(ActionListener.class, l);
    }
	
}
