package com.matt.difinspector.main;

import java.awt.AWTEvent;

public class ValueChangedEvent extends AWTEvent
{
	private static final long serialVersionUID = 2973887853882424869L;
	
	public static final int VALUE_CHANGED_FIRST = AWTEvent.RESERVED_ID_MAX + 1;
	
	public static final int VALUE_CHANGED_DEFAULT = VALUE_CHANGED_FIRST;

	public ValueChangedEvent(Object source, int id)
	{
		super(source, id);
	}

}
