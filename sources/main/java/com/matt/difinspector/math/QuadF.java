package com.matt.difinspector.math;

public class QuadF
{
	private Point3F point1, point2, point3, point4;
	
	public QuadF(Point3F point1, Point3F point2, Point3F point3, Point3F point4)
	{
		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;
		this.point4 = point4;
	}
	
	public TriangleF[] getTriangles()
	{
		TriangleF[] tris = new TriangleF[2];
		
		tris[0] = new TriangleF(this.point1, this.point2, this.point4);
		tris[1] = new TriangleF(this.point2, this.point3, this.point4);
		
		return tris;
	}
	
	public Point3F getPoint1()
	{
		return this.point1;
	}
	
	public Point3F getPoint2()
	{
		return this.point2;
	}
	
	public Point3F getPoint3()
	{
		return this.point3;
	}
	
	public Point3F getPoint4()
	{
		return this.point4;
	}
}
