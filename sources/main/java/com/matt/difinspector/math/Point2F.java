package com.matt.difinspector.math;

public class Point2F
{
	private float x, y;
	
	public Point2F(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Point2F(float xy)
	{
		this(xy, xy);
	}
	
	public Point2F()
	{
		this(0);
	}
	
	public float getX()
	{
		return this.x;
	}
	
	public void setX(float x)
	{
		this.x = x;
	}
	
	public float getY()
	{
		return this.y;
	}
	
	public void setY(float y)
	{
		this.y = y;
	}
	
	public Point2F add(Point2F r)
	{
		return new Point2F(this.x + r.x, this.y + r.y);
	}
	
	public Point2F add(float r)
	{
		return new Point2F(this.x + r, this.y + r);
	}
	
	public Point2F sub(Point2F r)
	{
		return new Point2F(this.x - r.x, this.y - r.y);
	}
	
	public Point2F sub(float r)
	{
		return new Point2F(this.x - r, this.y - r);
	}
	
	public Point2F mul(Point2F r)
	{
		return new Point2F(this.x * r.x, this.y * r.y);
	}
	
	public Point2F mul(float r)
	{
		return new Point2F(this.x * r, this.y * r);
	}
	
	public Point2F div(Point2F r)
	{
		return new Point2F(this.x / r.x, this.y / r.y);
	}
	
	public Point2F div(float r)
	{
		return new Point2F(this.x / r, this.y / r);
	}
	
	public float[] toArray()
	{
		return new float[]{this.x, this.y};
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Point2F))
			return false;
		
		Point2F other = (Point2F)o;
		
		return this.x == other.x && this.y == other.y;
	}
	
	@Override
	public String toString()
	{
		return "(Point2F){x: " + this.x + ", y: " + this.y + "}";
	}
}
