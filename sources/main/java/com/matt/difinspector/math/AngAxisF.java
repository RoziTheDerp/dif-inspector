package com.matt.difinspector.math;

public class AngAxisF
{
	protected Point3F axis;
	protected float angle;
	
	public AngAxisF(float x, float y, float z, float angle)
	{
		this(new Point3F(x, y, z), angle);
	}
	
	public AngAxisF()
	{
		this(1, 0, 0, 0);
	}
	
	public AngAxisF(Point3F axis, float angle)
	{
		this.axis = axis;
		this.angle = angle;
	}
	
	public AngAxisF(QuatF quaternion)
	{
		float cosHalfAngle = quaternion.w;
		float angle = (float)Math.acos(cosHalfAngle) * 2;
		float sinHalfAngle = (float)Math.sin(angle / 2);
		
		float x = quaternion.x / sinHalfAngle;
		float y = quaternion.y / sinHalfAngle;
		float z = quaternion.z / sinHalfAngle;
		
		this.axis = new Point3F(x, y, z);
		this.angle = (float) Math.toDegrees(angle);
	}
	
	public AngAxisF toDegrees()
	{
		return new AngAxisF(this.axis, (float)Math.toDegrees(this.angle));
	}
	
	public AngAxisF toRadians()
	{
		return new AngAxisF(this.axis, (float)Math.toRadians(this.angle));
	}
	
	public Point3F toEuler()
	{
		Point3F axis = new Point3F(this.axis.x, this.axis.y, this.axis.z);
		
		Point3F euler = axis.mul(-this.angle);
		
		return euler;
	}

	public Point3F getAxis()
	{
		return axis;
	}

	public void setAxis(Point3F axis)
	{
		this.axis = axis;
	}

	public float getAngle()
	{
		return angle;
	}

	public void setAngle(float angle)
	{
		this.angle = angle;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof AngAxisF))
			return false;
		
		AngAxisF other = (AngAxisF)o;
		
		return this.axis.equals(other.axis) && this.angle == other.angle;
	}
	
	@Override
	public String toString()
	{
		return "(AngAxisF){axis: " + this.axis.toString() + ", angle: " + this.angle + "}";
	}
}
