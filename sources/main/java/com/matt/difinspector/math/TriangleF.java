package com.matt.difinspector.math;

public class TriangleF
{
	private Point3F point1, point2, point3;
	
	public TriangleF(Point3F point1, Point3F point2, Point3F point3)
	{
		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;
	}
	
	public Point3F getPoint1()
	{
		return this.point1;
	}
	
	public Point3F getPoint2()
	{
		return this.point2;
	}
	
	public Point3F getPoint3()
	{
		return this.point3;
	}
}
