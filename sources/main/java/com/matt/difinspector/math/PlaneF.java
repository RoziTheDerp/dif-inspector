package com.matt.difinspector.math;

public class PlaneF extends Point3F
{
	private float d;
	private int normalIndex;
	
	public PlaneF(Point3F point, float d)
	{
		super(point.getX(), point.getY(), point.getZ());
		this.d = d;
		//if (Float.isNaN(this.d))
		//	this.d = 0;
	}
	
	public PlaneF(float x, float y, float z, float d)
	{
		this(new Point3F(x, y, z), d);
	}
	
	public PlaneF()
	{
		this(new Point3F(), 0);
	}
	
	public PlaneF(Point3F point1, Point3F point2)
	{
		this.set(point1, point2);
	}
	
	public PlaneF(Point3F point1, Point3F point2, Point3F point3)
	{
		this.set(point1, point2, point3);
	}
	
	public void set(Point3F point1, Point3F point2)
	{
		this.x = point2.x;
		this.y = point2.y;
		this.z = point2.z;
		this.normalize();
		
		this.d = -(point1.x * this.x + point1.y * this.y + point1.z * this.z);
	}
	
	public void set(Point3F point1, Point3F point2, Point3F point3)
	{
		Point3F v1 = point1.sub(point2);
		Point3F v2 = point3.sub(point2);
		Point3F res = v1.cross(v2);
		this.set(point2, res);
	}
	
	public float getD()
	{
		return this.d;
	}
	
	public void setD(float d)
	{
		this.d = d;
	}
	
	public float getDistanceToPoint(Point3F point)
	{
		return (this.x * point.x + this.y * point.y + this.z * point.z) + this.d;
	}
	
	public void setNormalIndex(int normalIndex)
	{
		this.normalIndex = normalIndex;
	}
	
	public int getNormalIndex()
	{
		return this.normalIndex;
	}
	
	/*public Point3F getPoint1()
	{
		if (this.d == 0)
		{
			System.err.println("Oops!");
			this.d = 32;
		}
		
		if (this.x == 1)
		{
			// XPos
			return new Point3F(this.d, this.d, this.d);
		} else if (this.x == -1)
		{
			// XNeg
			return new Point3F(this.d, -this.d, -this.d);
		} else if (this.y == 1)
		{
			// YPos
			return new Point3F(this.d, this.d, -this.d);
		} else if (this.y == -1)
		{
			// YNeg
			return new Point3F(this.d, -this.d, this.d);
		} else if (this.z == 1)
		{
			// ZPos
			return new Point3F(this.d, this.d, this.d);
		} else if (this.z == -1)
		{
			// ZNeg
			return new Point3F(-this.d, this.d, -this.d);
		} else {
			System.err.println("Invalid Plane!");
			return null;//new Point3F(this.d, this.d, this.d);
		}
	}
	
	public Point3F getPoint2()
	{
		if (this.d == 0)
		{
			System.err.println("Oops!");
			this.d = 32;
		}
		
		if (this.x == 1)
		{
			// XPos
			return new Point3F(-this.d, this.d, this.d);
		} else if (this.x == -1)
		{
			// XNeg
			return new Point3F(-this.d, -this.d, -this.d);
		} else if (this.y == 1)
		{
			// YPos
			return new Point3F(-this.d, this.d, -this.d);
		} else if (this.y == -1)
		{
			// YNeg
			return new Point3F(-this.d, -this.d, this.d);
		} else if (this.z == 1)
		{
			// ZPos
			return new Point3F(this.d, -this.d, this.d);
		} else if (this.z == -1)
		{
			// ZNeg
			return new Point3F(-this.d, -this.d, -this.d);
		} else {
			System.err.println("Invalid Plane!");
			return null;//new Point3F(this.d, this.d, this.d);
		}
	}
	
	public Point3F getPoint3()
	{
		if (this.d == 0)
		{
			System.err.println("Oops!");
			this.d = 32;
		}
		
		if (this.x == 1)
		{
			// XPos
			return new Point3F(-this.d, -this.d, this.d);
		} else if (this.x == -1)
		{
			// XNeg
			return new Point3F(-this.d, this.d, -this.d);
		} else if (this.y == 1)
		{
			// YPos
			return new Point3F(-this.d, this.d, this.d);
		} else if (this.y == -1)
		{
			// YNeg
			return new Point3F(-this.d, -this.d, -this.d);
		} else if (this.z == 1)
		{
			// ZPos
			return new Point3F(this.d, -this.d, -this.d);
		} else if (this.z == -1)
		{
			// ZNeg
			return new Point3F(-this.d, -this.d, this.d);
		} else {
			System.err.println("Invalid Plane!");
			return null;//new Point3F(this.d, this.d, this.d);
		}
	}*/
	
	public void scaleUp(float amount)
	{
		this.x *= amount;
		this.y *= amount;
		this.z *= amount;
		this.d *= amount;
	}
	
	public void scaleDown(float amount)
	{
		this.x /= amount;
		this.y /= amount;
		this.z /= amount;
		this.d /= amount;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof PlaneF))
			return false;
		
		PlaneF plane = (PlaneF)o;
		
		boolean xC = false;
		boolean yC = false;
		boolean zC = false;
		boolean dC = false;
		
		if ((Float.isNaN(this.x) && Float.isNaN(plane.x)) || this.x == plane.x)
			xC = true;
		if ((Float.isNaN(this.y) && Float.isNaN(plane.y)) || this.y == plane.y)
			yC = true;
		if ((Float.isNaN(this.z) && Float.isNaN(plane.z)) || this.z == plane.z)
			zC = true;
		if ((Float.isNaN(this.d) && Float.isNaN(plane.d)) || this.d == plane.d)
			dC = true;
		
		return xC && yC && zC && dC;
	}
	
	@Override
	public String toString()
	{
		return "(PlaneF){x: " + this.x + ", y: " + this.y + ", z: " + this.z + ", d: " + this.d + "}";
	}
}
